;;;; move-mixins.lisp

(cl:in-package #:chess)

(defclass move-mixin ()
  ()
  (:documentation "The superclass for all MOVE mixins."))

(defmacro define-move-mixin (name &key documentation)
  "Generate correct code for defining a new MOVE mixin class."
  (check-type name (and symbol (not keyword)))
  (check-type documentation simple-string)
  (let ((mixin-name (a:symbolicate 'move- name '-mixin)))
    `(defclass ,mixin-name (move-mixin)
       ()
       (:documentation ,documentation))))

(define-move-mixin diagonal
  :documentation "")

(define-move-mixin around-the-corner
  :documentation "")

(define-move-mixin forward
  :documentation "")

(define-move-mixin just-one-field
  :documentation "")

(define-move-mixin arbitrary-many-fields
  :documentation "")

(define-move-mixin any-direction
  :documentation "")

(define-move-mixin horizontally-or-vertically
  :documentation "")
