;;;; color-mixins.lisp

(cl:in-package #:chess)

(defclass color-mixin ()
  ()
  (:documentation "This mixin is just intended not directly to be used."))

(defmacro define-color-mixin (color-name)
  "We need this to trick the patched DEFCLASS."
  (check-type color-name (member black white))
  (let ((mixin-name (a:symbolicate color-name '-mixin)))
    `(cl:defclass ,mixin-name (color-mixin)
       ())))

(define-color-mixin black)

(define-color-mixin white)
