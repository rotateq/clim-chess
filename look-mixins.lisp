;;;; look-mixins.lisp

(cl:in-package #:chess)

(defclass look-mixin ()
  ((%glyph
    :initarg :glyph
    :initform (error "This thing needs a specified glyph.")
    :reader glyph
    :type character))
  (:documentation "How something shall look like. For now just characters."))
