;;;; packages.lisp

(cl:defpackage #:de.lispinq.chess
  (:use #:cl)
  (:nicknames #:chess)
  (:local-nicknames (#:a #:alexandria)))
