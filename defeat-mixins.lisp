;;;; defeat-mixins.lisp

(cl:in-package #:chess)

(defclass defeat-mixin () ())

(defclass defeat-diagonal-mixin (defeat-mixin) ())
