;;;; clim-chess.asd

(asdf:defsystem #:clim-chess
  :description "A simple application for playing chess via McCLIM."
  :author "Jan Münch <lispinq@protonmail.com>"
  :version "0.0.1"
  :license "LLGPL3"
  :depends-on (#:alexandria
               #+(or)
	       #:mcclim)
  :serial t
  :components ((:file "packages")
	       (:file "move-mixins")
	       (:file "color-mixins")
	       (:file "look-mixins")
	       (:file "defeat-mixins")
	       (:file "figures")
	       (:file "empty-mixins")
	       (:file "fields")
	       (:file "board")
	       (:file "generic-functions")))
