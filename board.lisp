;;;; board.lisp

(cl:in-package #:chess)

(defclass board ()
  ()
  (:documentation "The abstract class for a play board."))

(defclass chess-board (board)
  ((%data
    :type (simple-array t (8 8))))
  (:documentation "The actual class for a chess board. We represent the data as a 8×8 matrix."))
