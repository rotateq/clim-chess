;;;; figures.lisp

(cl:in-package #:chess)

(deftype figure-name ()
  "The allowed names for the DEFINE-FIGURE macro."
  '(member king queen rook bishop knight pawn))

(defclass figure () ())

(defmacro define-figure (name &key mixins documentation)
  (check-type name figure-name)
  (check-type documentation simple-string)
  (let* ((black-figure-class-name (a:symbolicate 'black- name))
         (black-figure-char-name (format nil "BLACK_CHESS_~a" name))
         (black-figure-glyph (name-char black-figure-char-name))
         (white-figure-class-name (a:symbolicate 'white- name))
         (white-figure-char-name (format nil "WHITE_CHESS_~a" name))
         (white-figure-glyph (name-char white-figure-char-name)))
    `(progn
       (defclass ,name (figure ,@mixins)
         ()
         (:documentation ,documentation))
       (defclass ,black-figure-class-name (,name black-mixin look-mixin)
         ()
         (:default-initargs
          :glyph ,black-figure-glyph))
       (defclass ,white-figure-class-name (,name white-mixin look-mixin)
         ()
         (:default-initargs
          :glyph ,white-figure-glyph)))))

(define-figure king
  :mixins (move-just-one-field-mixin
           move-any-direction-mixin)
  :documentation "One instance per color allowed for a game.")

(define-figure queen
  :mixins (move-forward-mixin
           move-arbitrary-many-fields-mixin
           move-diagonal-mixin)
  :documentation "One instance per color allowed for a game.")

(define-figure rook
  :mixins (move-horizontally-or-vertically-mixin)
  :documentation "Two instances per color allowed for a game.")

(define-figure bishop
  :mixins (move-diagonal-mixin)
  :documentation "Two instances per color allowed for a game.")

(define-figure knight
  :mixins (move-around-the-corner-mixin)
  :documentation "Two instances per color allowed for a game.")

(define-figure pawn
  :mixins (move-just-one-field-mixin
           move-forward-mixin
           defeat-diagonal-mixin)
  :documentation "Eight instances per color allowed for a game.")
