;;;; generic-functions.lisp

(cl:in-package #:chess)

(defgeneric defeat (figure-1 figure-2))
