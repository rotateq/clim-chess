;;;; fields.lisp

(cl:in-package #:chess)

(defclass field ()
  ())

(defclass black-field (field black-mixin look-mixin)
  ()
  (:default-initargs
   :glyph #\x))

(defclass empty-black-field (black-field empty-mixin)
  ())

(defclass non-empty-black-field (black-field non-empty-mixin)
  ())

(defclass white-field (field white-mixin look-mixin)
  ()
  (:default-initargs
   :glyph #\o))

(defclass empty-white-field (white-field empty-mixin)
  ())

(defclass non-empty-white-field (white-field non-empty-mixin)
  ())
